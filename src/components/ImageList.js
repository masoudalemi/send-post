import React from 'react'
import './ImageList.css'

function ImageList(props) {
  const { images, removeImage } = props

  const grid = images.map(({ id, asset }) => {
    return (
      <div
        key={id}
        style={{
          backgroundImage: `url(${asset})`,
        }}>
        <div
          key={`remove-${id}`}
          className="cross-button"
          onClick={() => {
            removeImage(id)
          }}>
          ✖
        </div>
      </div>
    )
  })
  return (
    <div data-testid="ImageList" className="image-list">
      {grid}
    </div>
  )
}

export default ImageList
