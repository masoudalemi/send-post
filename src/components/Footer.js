import React from 'react'
import './Footer.css'
import chartIcon from './../assets/images/icon-chart.png'
import videoIcon from './../assets/images/icon-video.png'
import imageIcon from './../assets/images/icon-image.png'

function Footer(props) {
  const { submit } = props

  return (
    <div className="footer-root">
      <div className="button-container">
        <div onClick={submit}>انتشار</div>
      </div>
      <div className="icon-container">
        <div>
          <img className="icon-chart" src={chartIcon} alt="attach chart" />
        </div>
        <div>
          <img className="icon-video" src={videoIcon} alt="attach video" />
        </div>
        <div>
          <img className="icon-image" src={imageIcon} alt="attach graphics" />
        </div>
      </div>
    </div>
  )
}

export default Footer
