import React, { useState } from 'react'
import './SendPost.css'
import Avatar from './Avatar'
import ProfileSelect from './ProfileSelect'
import ProfileInput from './ProfileInput'
import ImageList from './ImageList'
import bedImage from './../assets/images/bed.jpg'
import lightImage from './../assets/images/light.jpg'
import ficusImage from './../assets/images/ficus.jpg'
import Footer from './Footer'

function SendPost() {
  const [title, setTitle] = useState('')
  const [body, setBody] = useState('')
  const [images, setImages] = useState([
    { id: 1, asset: lightImage },
    { id: 2, asset: bedImage },
    { id: 3, asset: ficusImage },
  ])
  const [categoryIndex, setCategoryIndex] = useState(null)
  const [categories, setCategories] = useState([
    {
      id: 1,
      name: 'مدیریت محصول',
    },
    {
      id: 2,
      name: 'طراحی محصول',
    },
    {
      id: 3,
      name: 'هنر',
    },
  ])

  /**
   * Adds a new category
   * @param {String} name Name of the new category
   */
  const addCategory = name => {
    setCategories([{ id: categories.length + 1, name }].concat(categories))
    setCategoryIndex(0)
  }

  /**
   * Removes a image from list by Id
   * @param {Number} id Id that passed along with the image
   */
  const removeImage = id => setImages(images.filter(item => item.id !== id))

  const submit = () =>
    console.log('Here is a sample data that can be submitted to API', {
      title,
      body,
      images,
      category: categories[categoryIndex],
    })

  return (
    <form action="">
      <Avatar />
      <ProfileSelect
        options={categories}
        placeholder="انتخاب دسته‌بندی"
        addOptionText="دسته‌بندی جدید"
        addOptionPlaceholder="عنوان دسته‌بندی"
        addOptionButtonText="ایجاد"
        value={categoryIndex}
        onChange={setCategoryIndex}
        addOption={addCategory}
      />
      <ProfileInput
        name="title"
        type="text"
        placeholder="عنوان مطلب"
        value={title}
        onChange={setTitle}
        cssClasses="title-container"
      />
      <ProfileInput
        name="body"
        type="textarea"
        placeholder="متن مورد نظر خود را تایپ کنید"
        value={body}
        onChange={setBody}
        cssClasses="body-container"
      />
      <ImageList images={images} removeImage={removeImage} />
      <Footer submit={submit} />
    </form>
  )
}

export default SendPost
