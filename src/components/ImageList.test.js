import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import ImageList from './ImageList'
import bedImage from './../assets/images/bed.jpg'
import lightImage from './../assets/images/light.jpg'
import ficusImage from './../assets/images/ficus.jpg'

let images = [
  { id: 1, asset: lightImage },
  { id: 2, asset: bedImage },
  { id: 3, asset: ficusImage },
]
const removeImage = id => (images = images.filter(item => item.id !== id))

test('Displays Images', async () => {
  const { queryAllByText } = render(
    <ImageList images={images} removeImage={removeImage} />,
  )
  expect(queryAllByText('✖')).toHaveLength(3)
})

test('Can remove images', async () => {
  const { queryAllByText, rerender } = render(
    <ImageList images={images} removeImage={removeImage} />,
  )
  const randomIndex = Math.floor(Math.random() * (images.length - 0.1))
  const buttons = queryAllByText('✖')
  fireEvent.click(buttons[randomIndex])

  rerender(<ImageList images={images} removeImage={removeImage} />)

  expect(screen.queryAllByText('✖')).toHaveLength(images.length)
})
