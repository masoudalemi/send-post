import React, { useState } from 'react'
import './ProfileSelect.css'
import ProfileInput from './ProfileInput'

function ProfileSelect(props) {
  const [folded, setFolded] = useState(true)
  const [adding, setAdding] = useState(false)
  const [newCategory, setNewCategory] = useState('')
  const [hasError, setHasError] = useState(false)

  const {
    placeholder = '',
    addOptionText = '',
    addOptionButtonText = '',
    addOptionPlaceholder = '',
    options = [],
    value = null,
    onChange = () => {},
    addOption = () => {},
  } = props

  /**
   * Fold/Unfolds the component
   */
  const toggleFold = () => setFolded(!folded)

  const fold = () => {
    toggleFold()
    setAdding(false)
    setNewCategory('')
    setHasError(false)
  }

  /**
   * Updates input value and validation status
   * @param {String} name User input for new category
   */
  const handleInputUpdate = name => {
    setNewCategory(name)
    setHasError(name.length > 0 ? false : true)
  }

  /**
   * Updates validation status and Fires addOption if input is valid
   */
  const handleButtonClick = () => {
    if (newCategory.length > 0) {
      setNewCategory('')
      setHasError(false)
      setAdding(false)
      setFolded(true)
      addOption(newCategory)
    } else {
      setHasError(true)
    }
  }

  const passedOptions =
    folded === true || adding === true
      ? null
      : options.map(({ id, name }, index) => (
          <div
            className="select-option"
            key={id}
            onClick={() => {
              onChange(index)
              toggleFold()
            }}>
            <div className="option-text">{name}</div>
            <div className="option-icon"></div>
          </div>
        ))

  const addOptionRow =
    folded === true || adding === true ? null : (
      <div
        className="select-option non-selectable add-option"
        key="add-option"
        onClick={() => {
          setAdding(true)
        }}>
        <div className="option-text">{addOptionText}</div>
        <div className="option-icon">
          <span className="plus">✖</span>
        </div>
      </div>
    )

  const addOptionForm =
    adding === true && folded === false
      ? [
          <ProfileInput
            name="new-category"
            type="text"
            placeholder={addOptionPlaceholder}
            value={newCategory}
            onChange={handleInputUpdate}
            cssClasses={`new-category-container ${
              hasError === true ? 'has-error' : ''
            }`}
            key="add-input-container"
          />,
          <div className="non-option" key="add-button-container">
            <div className="button" onClick={handleButtonClick}>
              {addOptionButtonText}
            </div>
          </div>,
        ]
      : null

  const selectedText =
    adding === true
      ? addOptionText
      : value === null
      ? placeholder
      : options[value].name

  return (
    <div className={`select-container ${folded ? 'folded' : ''}`}>
      <div className="select-option non-selectable" onClick={fold}>
        <div className="option-text">{selectedText}</div>
        <div className="option-icon">
          <i className={`arrow ${folded ? 'down' : 'up'}`}></i>
        </div>
      </div>
      {passedOptions}
      {addOptionRow}
      {addOptionForm}
    </div>
  )
}

export default ProfileSelect
