import React from 'react'
import './ProfileInput.css'

const ProfileInput = props => {
  const {
    name,
    value,
    placeholder = '',
    type = 'text',
    onChange = () => {},
    cssClasses = '',
    maxHeight,
  } = props

  const handleChange = event => {
    if (type === 'textarea') {
      resizeTextArea(event)
    }
    onChange(event.target.value)
  }

  const resizeTextArea = event => {
    event.target.style.height = 'inherit'
    let height =
      typeof maxHeight === 'number' && event.target.scrollHeight > maxHeight
        ? maxHeight
        : event.target.scrollHeight
    event.target.style.height = `${height}px`
  }

  const fieldProps = {
    name,
    type,
    value,
    placeholder,
    onChange: handleChange,
  }

  if (type === 'textarea') {
    delete fieldProps.type
    delete fieldProps.value

    fieldProps.defaultValue = value
  }

  return (
    <div className={`${cssClasses} profile-input-container`}>
      {type === 'textarea' ? (
        <textarea {...fieldProps} />
      ) : (
        <input {...fieldProps} />
      )}
    </div>
  )
}

export default ProfileInput
