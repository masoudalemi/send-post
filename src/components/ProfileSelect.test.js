import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import ProfileSelect from './ProfileSelect'

let options = [
  { name: 'one', id: 1 },
  { name: 'two', id: 2 },
]

test('Displays Placeholder', () => {
  const { findByText } = render(
    <ProfileSelect placeholder="Choose an option" />,
  )
  findByText('Choose an option')
})

test('Displays all options on unfold', async () => {
  const { getByText, queryAllByText } = render(
    <ProfileSelect
      options={options}
      placeholder="Choose an option"
      addOptionText="Add new option"
    />,
  )

  fireEvent.click(getByText('Choose an option'))

  expect(queryAllByText('one')).toHaveLength(1)
  expect(queryAllByText('two')).toHaveLength(1)
  expect(queryAllByText('Choose an option')).toHaveLength(1)
  expect(queryAllByText('Add new option')).toHaveLength(1)
})

test('Option selection works', async () => {
  let selectedIndex = null
  const setSelectedIndex = newValue => {
    selectedIndex = newValue
  }

  const { rerender } = render(
    <ProfileSelect
      options={options}
      placeholder="Choose an option"
      addOptionText="Add new option"
      value={selectedIndex}
      onChange={setSelectedIndex}
    />,
  )

  fireEvent.click(screen.getByText('Choose an option'))
  fireEvent.click(screen.getByText('two'))

  rerender(
    <ProfileSelect
      options={options}
      placeholder="Choose an option"
      addOptionText="Add new option"
      value={selectedIndex}
      onChange={setSelectedIndex}
    />,
  )

  expect(screen.queryAllByText('one')).toHaveLength(0)
  expect(screen.queryAllByText('Add new option')).toHaveLength(0)
  expect(screen.queryAllByText('Choose an option')).toHaveLength(0)
  expect(screen.queryAllByText('two')).toHaveLength(1)
})

test('Can add new option', async () => {
  let selectedIndex = null
  const setSelectedIndex = newValue => {
    selectedIndex = newValue
  }

  let options = []
  const addOption = name => {
    options = [{ id: options.length + 1, name }].concat(options)
    setSelectedIndex(0)
  }

  const { rerender } = render(
    <ProfileSelect
      options={options}
      placeholder="Choose an option"
      addOptionText="Add new option"
      addOptionButtonText="Add and select option"
      addOptionPlaceholder="New option"
      value={selectedIndex}
      onChange={setSelectedIndex}
      addOption={addOption}
    />,
  )

  fireEvent.click(screen.getByText('Choose an option'))
  fireEvent.click(screen.getByText('Add new option'))

  fireEvent.change(screen.getByPlaceholderText('New option'), {
    target: { value: 'test option' },
  })
  fireEvent.click(screen.getByText('Add and select option'))

  rerender(
    <ProfileSelect
      options={options}
      placeholder="Choose an option"
      addOptionText="Add new option"
      addOptionButtonText="Add and select option"
      addOptionPlaceholder="New option"
      value={selectedIndex}
      onChange={setSelectedIndex}
      addOption={addOption}
    />,
  )

  expect(screen.queryAllByText('Add new option')).toHaveLength(0)
  expect(screen.queryAllByText('Choose an option')).toHaveLength(0)
  expect(screen.queryAllByText('test option')).toHaveLength(1)
})
