import React from 'react'
import { render, screen } from '@testing-library/react'
import SendPost from './SendPost'

const { queryAllByText } = render(<SendPost />)

test('Renders a <Avatar />', () => {
  const avatars = screen.getAllByTestId('Avatar')
  expect(avatars).toHaveLength(1)
})

test('Renders a <ProfileSelect /> for categories', () => {
  screen.findByText('انتخاب دسته‌بندی')
})

test('Renders two <ProfileInput />, one for title and another for body', () => {
  screen.findByPlaceholderText('عنوان مطلب')
  screen.findByPlaceholderText('متن مورد نظر خود را تایپ کنید')
})

test('Renders a <ImageList />', () => {
  screen.queryByTestId('ImageList')
})

test('Renders a <Footer />', () => {
  screen.findByText('انتشار')
})
