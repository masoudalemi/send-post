import React from 'react'
import './Avatar.css'
import author from './../assets/images/author.png'

function Avatar() {
  return (
    <div data-testid="Avatar" id="avatar-container">
      <img id="avatar" src={author} alt="Avatar" />
    </div>
  )
}

export default Avatar
